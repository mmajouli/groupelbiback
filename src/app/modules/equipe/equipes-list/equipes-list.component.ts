import {Component, OnInit} from '@angular/core';
import equipe from 'src/assets/equipe.json';
import myequipe from 'src/assets/equipe1.json';
import multiobject from 'src/assets/multiobject.json';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-equipes-list',
  templateUrl: './equipes-list.component.html',
  styleUrls: ['./equipes-list.component.css']
})
export class EquipesListComponent implements OnInit {
  equipess: any = equipe;
  myequipes: [] = myequipe;
  resultat: [] = [];
  test = 'animation1';
  listeHeadertable: [] = [];

  multiobjects = multiobject;

  constructor() {
    let obj;
    let objects;
    console.log(this.multiobjects);
    for (objects in this.multiobjects) {
      // console.log(objects);
      this.listeHeadertable.push(objects);
    }
    // afficher les titre recuprer du fichier json
    console.log(this.listeHeadertable);
    // test json test
    // this.resultat = this.getThatArray(this.multiobjects[this.test]);
    // parcour sur les titres
    for (objects in this.multiobjects) {
      this.resultat.push({[objects]: this.getThatArray(this.multiobjects[objects])});
    }
    console.log(this.resultat);
    // on verra apres
    for (objects in this.resultat) {
      console.log(objects + ':' + this.resultat[objects]);
    }
    for (objects in this.multiobjects[this.test]) {
       // console.log("l'objet " + objects);
     for ( obj in this.multiobjects[this.test][objects]) {
       // console.log(console.log(obj + ' : ' + this.multiobjects[this.test][objects][obj] ));
     }
    }

  }


  ngOnInit() {

  }

  toArray(answers: object) {
    return Object.keys(answers).map(key => answers[key]);
  }

  getThatArray(tableau: object) {
    const thtable = [];
    let objects;
    let obj;
    for (objects in tableau) {
      console.log("l'objet " + objects);
      for( obj in tableau[objects]) {
        thtable.push(obj);
      }
      break;
    }
    return thtable;
  }

}
