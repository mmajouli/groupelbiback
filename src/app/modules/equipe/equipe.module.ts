import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EquipesComponent } from './equipes/equipes.component';
import { EquipesListComponent } from './equipes-list/equipes-list.component';
import {EquipeService} from '../shared/equipe.service';


@NgModule({
  declarations: [EquipesComponent, EquipesListComponent],
  exports: [
    EquipesListComponent,
    EquipesComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [EquipeService]
})
export class EquipeModule { }
