import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EquipeModel} from '../model/equipe.model';
const headerOption = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable()
export class EquipeService {

  mockUrel = 'http://localhost:3000/equipe';
  constructor(
    private http: HttpClient
  ) { }
  getAllEquipe(): Observable <EquipeModel[]> {
    return this.http.get<EquipeModel[]>(this.mockUrel, headerOption);
  }
}
